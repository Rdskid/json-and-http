import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.*;
import java.net.*;
public class ClientApp {
    public static String toHTTP(HTTP http){
        String s = "";
        try {
            URL url = new URL("http://www.google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            s = stringBuilder.toString();

        } catch (Exception IOException) {
            System.out.println(IOException);

        }

        ObjectMapper mapper = new ObjectMapper();

        try {
            s = mapper.writeValueAsString(http);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }
    public static HTTP fromHTTP(String s){
        ObjectMapper mapper = new ObjectMapper();
        HTTP http = null;

        try {
            http = mapper.readValue(s, HTTP.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return http;
    }

    public static void main(String[] args){

        HTTP application = new HTTP();
        application.setName("Ryan");
        application.setNumber(1234567890);

        String json = ClientApp.toHTTP(application);
        System.out.println(json);

        HTTP app2 = ClientApp.fromHTTP(json);
        System.out.println(app2);


    }


}

