public class HTTP {

    public String name;
    public Integer number;

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public Integer getNumber(){
        return number;
    }
    public void setNumber(Integer number){
        this.number = number;
    }
    public String toString(){
        return "Name: " + name + "\nNumber: " + number;
    }

}
